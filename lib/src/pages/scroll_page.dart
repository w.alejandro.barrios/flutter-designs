import 'package:flutter/material.dart';

class ScrollPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        scrollDirection: Axis.vertical,
        children: <Widget>[_page1(), _page2(context)],
      ),
    );
  }

  Widget _page1() {
    return Stack(
      children: <Widget>[_colorFondo(), _imagenFondo(), _textos()],
    );
  }

  Widget _colorFondo() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Color.fromRGBO(108, 192, 218, 1.0),
    );
  }

  Widget _imagenFondo() {
    return Container(
        width: double.infinity,
        height: double.infinity,
        child: Image(
            image: AssetImage('assets/img/original.png'), fit: BoxFit.cover));
  }

  Widget _textos() {
    final estiloTexto = TextStyle(color: Colors.white, fontSize: 50.0);

    return SafeArea(
      child: Column(
        children: <Widget>[
          SizedBox(height: 15.0),
          Text('11°', style: estiloTexto),
          Text('Miércoles', style: estiloTexto),
          Expanded(child: Container()),
          Icon(Icons.keyboard_arrow_down, size: 70.0, color: Colors.white)
        ],
      ),
    );
  }

  Widget _page2(BuildContext context) {
    return Stack(
      children: <Widget>[_colorFondo(), _button(context)],
    );
  }

  Widget _button(BuildContext context) {
    return Container(
      child: Center(
        child: RaisedButton(
          shape: StadiumBorder(),
            padding: EdgeInsets.symmetric(horizontal: 50.0),
            color: Colors.blueAccent,
            onPressed: () {
              Navigator.pushNamed(context, 'botones');
            },
            child: Text('Bienvenido',
                style: TextStyle(color: Colors.white, fontSize: 20.0))),
      ),
    );
  }
}
