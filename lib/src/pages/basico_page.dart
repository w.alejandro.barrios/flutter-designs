import 'package:flutter/material.dart';

class BasicoPage extends StatelessWidget {
  final estiloTitulo = TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold);
  final estiloSubtitulo = TextStyle(fontSize: 16.0, color: Colors.grey);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          _buildImage(),
          _buildTitle(),
          _buildIcons(context),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
          _buildText(),
        ]),
      ),
    );
  }

  Widget _buildImage() {
    return FadeInImage(
        fit: BoxFit.cover,
        placeholder: AssetImage('assets/img/loading.gif'),
        image: NetworkImage(
            'https://upload.wikimedia.org/wikipedia/commons/3/35/Neckertal_20150527-6384.jpg'));
  }

  Widget _buildTitle() {
    final row = Row(
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Landscape Natural', style: estiloTitulo),
              SizedBox(height: 7.0),
              Text('Landscape de algun lugar del mundo',
                  style: estiloSubtitulo)
            ],
          ),
        ),
        Icon(Icons.star, color: Colors.red, size: 30.0),
        Text('41', style: TextStyle(fontSize: 20.0))
      ],
    );

    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
        child: row,
      ),
    );
  }

  Widget _buildIcons(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _icon(Icons.call, 'Call', context),
        _icon(Icons.near_me, 'Route', context),
        _icon(Icons.share, 'Share', context)
      ],
    );
  }

  Widget _icon(IconData icon, String text, BuildContext context) {
    return GestureDetector(
      child: Column(
        children: <Widget>[
          Icon(icon, color: Colors.blue, size: 30.0),
          SizedBox(height: 5.0),
          Text(text, style: TextStyle(fontSize: 15.0, color: Colors.blue))
        ],
      ),
      onTap: () {
        Navigator.pushNamed(context, 'scroll');
      },
    );
  }

  Widget _buildText() {
    return SafeArea(
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 40.0),
          child: Text(
              'Consectetur eiusmod do exercitation velit consectetur esse excepteur. Reprehenderit deserunt dolor nisi sit duis do id aliqua cillum qui sit. Cillum sint quis commodo quis.',
              textAlign: TextAlign.justify)),
    );
  }
}
