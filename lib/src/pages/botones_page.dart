import 'package:flutter/material.dart';

import 'dart:math';
import 'dart:ui';

class BotonesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _fondoApp(),
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _titulos(),
                _botonesRedondeados()
              ],
            ),
          )
        ],
      ),
      bottomNavigationBar: _bottomNavigationBar(context),
    );
  }

  Widget _fondoApp() {
    final gradiente = Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: FractionalOffset(0.0, 0.6),
              end: FractionalOffset(0.0, 1.0),
              colors: [
            Color.fromRGBO(50, 54, 101, 1.0),
            Color.fromRGBO(35, 37, 57, 1.0)
          ])),
    );

    final cajaRosada = Transform.rotate(
        // Controla rotacion
        angle: -pi / 5.0,
        child: Container(
          height: 360.0,
          width: 360.0,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(80.0),
              gradient: LinearGradient(colors: [
                Color.fromRGBO(236, 98, 188, 1.0),
                Color.fromRGBO(241, 142, 172, 1.0)
              ])),
        ));

    return Stack(
      children: <Widget>[
        gradiente,
        Positioned(
            // Cambia la posicion
            top: -100,
            child: cajaRosada)
      ],
    );
  }

  Widget _titulos() {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Classify transaction',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold)),
            SizedBox(height: 10.0),
            Container(
              width: 200.0,
              child: Text(
                  'Classify this transaction into a particular category',
                  style: TextStyle(color: Colors.white, fontSize: 18.0)),
            )
          ],
        ),
      ),
    );
  }

  Widget _bottomNavigation() {
    return BottomNavigationBar(
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.calendar_today),
          title: Container()
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.pie_chart_outlined),
          title: Container()
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.supervised_user_circle),
          title: Container()
        )
      ], 
    );
  }

  Widget _bottomNavigationBar(BuildContext context) {

    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Color.fromRGBO(55, 57, 84, 1.0),
        primaryColor: Colors.pinkAccent,
        textTheme: Theme.of(context).textTheme.copyWith(
          caption: TextStyle(color: Color.fromRGBO(116, 117, 152, 1.0))
        )
      ),
      child: _bottomNavigation()
    );

  }

  Widget _botonesRedondeados() {
    return Table(
      children: [
        _tableRow([Colors.blue, Colors.pinkAccent], [Icons.shop, Icons.card_giftcard], ['Shop', 'Gift Card']),
        _tableRow([Colors.green, Colors.orange], [Icons.mail, Icons.event], ['E-Mails', 'Events']),
        _tableRow([Colors.deepPurple, Colors.deepOrange], [Icons.exposure_zero, Icons.flight], ['Zero', 'Flight']),
        _tableRow([Colors.lime, Colors.indigoAccent], [Icons.nature, Icons.computer], ['Nature', 'Computer']),
      ],
    );
  }

  TableRow _tableRow(List<Color> colors, List<IconData> icons, List<String> textos) {
    return TableRow(
      children: [
        _crearBotonRedondeado(colors[0], icons[0], textos[0]),
        _crearBotonRedondeado(colors[1], icons[1], textos[1]),
      ]
    );
  }

  Widget _crearBotonRedondeado(Color color, IconData icon, String texto) {

    final container = Container(
      height: 180.0,
      margin: EdgeInsets.all(15.0),
      decoration: BoxDecoration(
        color: Color.fromRGBO(62, 66, 107, 0.7),
        borderRadius: BorderRadius.circular(30.0)
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          SizedBox(height: 5.0),
          CircleAvatar(
            backgroundColor: color,
            radius: 35.0,
            child: Icon(icon, color: Colors.white, size: 30.0),
          ),
          Text(texto, style: TextStyle(color: color)),
          SizedBox(height: 5.0)
        ],
      ),
    );

    /*return BackdropFilter( // Uso de blur
      filter: ImageFilter.blur(
        sigmaX: 1.0,
        sigmaY: 1.0
      ),
      child: container,
    );*/

    return container;
  }

}
